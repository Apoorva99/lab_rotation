import numpy as np


def onedim_to_twodim(k,m,n):
    i = k // m + 1 - 1
    j = k % m 
    return i,j


def twodim_to_onedim (i,j,m,n):
    i = i + 1
    j = j +1
    k = (i-1) * n + j -1 
    return k


def find_nth_neigh_general (k,m,n,nth):
    i,j = onedim_to_twodim(k,m,n)
    
    i_up_all = []
    i_down_all = []
    for ct in (np.arange(nth)+1):
        i_up = int(i-ct >= 0) * (i-ct) + (m-(ct-i)) * int(i-ct < 0)
        i_down = int(i+ct <= m-1) * (i+ct) + (ct - ((m-1)-i)-1) * int(i+ct > m-1)
        i_up_all.append(i_up)
        i_down_all.append(i_down)
        
    j_left_all = []
    j_right_all = []
    for ct in (np.arange(nth)+1):
        j_left = int(j-ct >= 0) * (j-ct) + (n-(ct-j)) * int(j-ct < 0)
        j_right = int(j+ct <= n-1) * (j+ct) + (ct - ((n-1)-j)-1) * int(j+ct > n-1)
        j_left_all.append(j_left)
        j_right_all.append(j_right)
        
    x = [i_up_all[-1]]*(2*nth+1)
    y = [i_down_all[-1]]*(2*nth+1)
    z = i_up_all[:-1] + [i] + i_down_all[:-1] 
    NB_i = np.array(x + y + z +z)
    
    xx = [j_right_all[-1]]*(2*nth-1)
    yy = [j_left_all[-1]]*(2*nth-1)
    zz = j_left_all + [j] + j_right_all 
    NB_j = np.array(zz + zz + xx + yy)
    NB = twodim_to_onedim (NB_i,NB_j,m,n)
    return NB

def find_allneigh(n,m,num_neigh):
    # n and m are lattice dimensions
    # num_neigh is the maximum number of nearest neighbors(first,second,etc.)
    num_cell = n*m
    neigh_all = []
    for i in range(num_cell):
        temp_NB = []
        for j in range(num_neigh):
            NB = find_nth_neigh_general(i,m,n,j+1)
            temp_NB.append(NB)
        neigh_all.append(temp_NB)
    return neigh_all


def update_network_states_ref0_conv (k,m,n,s,p,neigh_all):
    pot_s = 0
    for i in range(len(p)):
        if i==0:
            d = np.random.rand()
            pot_s = pot_s + int(d < p[0])
        else:             
            NB = neigh_all[k][i-1]  
            active_NB = sum(s[NB]>0)

            for j in range(active_NB):
                d = np.random.rand()
                if i==2:
                    pot_s = pot_s + int(d < p[i][k])
                else:
                    pot_s = pot_s + int(d < p[i])
    
    s_new =  int(pot_s>0)
    return s_new, pot_s