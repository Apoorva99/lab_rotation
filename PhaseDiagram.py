#Generate Phase Diagrams data file

import sys
import argparse
import numpy as np
import numpy as np
from latticeActivity import *
from GenGraph import *


parser = argparse.ArgumentParser()
parser.add_argument('--L', type=int, default=32, help='Network Size L')
parser.add_argument('--mode', type=str, default='ind', help='normalize by avg: avg or normalize by individual degree: ind')

args = parser.parse_args()


def update_network_states_ref0_conv (k,m,n,s,p,neigh_all):
    pot_s = 0
    #p_notActive = 1
    for i in range(len(p)):
        if i==0:
            #p_notActive = p_notActive * (1-p[0])
            d = np.random.rand()
            pot_s = pot_s + int(d < p[0])
        else:             
            NB = neigh_all[k][i-1]  
            active_NB = sum(s[NB]>0)

            for j  in range(active_NB):
                d = np.random.rand()
                if i==2:
                    pot_s = pot_s + int(d < p[i][k])
                else:
                    pot_s = pot_s + int(d < p[i])
    
    s_new =  int(pot_s>0)
    return s_new


def Phase_Diagram(m,n,ref,sigma_vec,pext,p0,num_its,num_meas,relax_its):


    # m*n is the lattice size
    # ref is refractory period
    # sigma_vec are the branching parameters we wish to obtain
    # pext is the probability for spontaneous firing
    # num_neigh is the number of nearest neighbors which should be considered in model
    # num_its is the number of iterations we want to use for each branching value
    # num_meas gives how much iteration we skip between two measurements
    # relax_its is how much we wait before starting measurements
    # ptype (is string) shows which type of probability distribution we should consider for neighboring activation
    # self_exciteP if 1 shows we consider self_excitation in probabilities
    # self_excite_neigh if 1 shows we have self-excitation in model (each cell is the first neighbor of itself)
    # direc shows the directory in which we should save the results

    G = get_scale_free(m,n,distance_metric,5,-0.1,1)            
    
    total_deg = 0
    avg_deg = 0

    for f in G.nodes():
        total_deg = total_deg + G.degree(f)

    avg_deg = total_deg/(m*n)

    output = open("pd-data/pd-"+str(m)+"-zoomed-"+str(args.mode), "w+")
    
    for j,sigma in enumerate(sigma_vec):

        neigh_all = []
        pneigh = []

        for i in G.nodes():
            neigh_i = [n for n in G.neighbors(i)]
            neigh_all.append([[i],neigh_i])
            if args.mode == 'ind':
                pneigh.append((sigma-p0)/(len(neigh_i)+1))  # +1 for self excitation
            else:
                pneigh.append((sigma-p0)/(avg_deg+1))  # +1 for self excitation

        p = [pext, p0, pneigh]

        s = np.zeros(m*n, dtype=int)
        index_ones = int(0.1*(m*n))
        s[:index_ones] = 1
        np.random.shuffle(s)
        active = s.sum()

        #To store the activity at each time
        current_activity = np.zeros(num_its // num_meas)

        #Thermalization steps
        t = 0
        while (t < relax_its and active > 0):
            if ref == 0:
                s = np.array([update_network_states_ref0_conv(k,m,n,s,p,neigh_all) for k in range(0,len(s))])     

            active = s.sum()
            t += 1
        
        #Measurement steps

        if active > 0: 
            t = 0
            while (t < num_its and active > 0):
                #Update lattice
                if ref == 0:
                    s = np.array([update_network_states_ref0_conv(k,m,n,s,p,neigh_all) for k in range(0,len(s))])     
                #Measure activity at this time
                active = s.sum()
                if t % num_meas == 0:
                    current_activity[t // num_meas] = active / (n*m)
                
                #Next time
                t += 1

            #If activity survived enough time, we are either supercritical or either approaching criticality
            #rho_s[j,0] = current_activity.mean()
            #rho_s[j,1] = current_activity.var() 
            output.write("{s} {rho} {v}\n".format(s=sigma, rho=current_activity.mean(), v=current_activity.var()))

        else:
            #If we fell during thermalization in absorbing state, then without any doubt...
            #rho_s[j,0] = 0.0
            #rho_s[j,1] = 0.0
            output.write("{s} {rho} {v}\n".format(s=sigma, rho=0.0, v=0.0))

    #np.save("phase_diagram", rho_s)
    output.close()


m = args.L
n = args.L

# refractory period, put to 0 for this code
ref = 0

# branching ratio
sigma_vec = np.linspace(0.6, 1.0, 100)

# external input, put to 0 for avalanches
pext = 0

# self-excitaion (p_s in thesis)
p0 = 0.5

# number of iterations
num_its = 1000
num_meas = 10
relax_its = 70


#Single phase diagram
Phase_Diagram(m,n,ref,sigma_vec,pext,p0,num_its,num_meas,relax_its)


