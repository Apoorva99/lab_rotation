#!/bin/bash
#SBATCH --partition=cpu-long
#SBATCH --ntasks=1                # Number of tasks (see below)
#SBATCH --cpus-per-task=1         # Number of CPU cores per task
#SBATCH --nodes=1                 # Ensure that all cores are on one machine
#SBATCH --time=2-23:00          # Runtime in D-HH:MM
#SBATCH --mem=64G                # Memory pool for all cores (see also --mem-per-cpu)
#SBATCH --output=/mnt/qb/work/levina/lfz197/log/hostname_%j.out  # File to which STDOUT will be written - make sure this is not on $HOME
#SBATCH --error=/mnt/qb/work/levina/lfz197/log/hostname_%j.err   # File to which STDERR will be written - make sure this is not on $HOME
scontrol show job $SLURM_JOB_ID
source /etc/profile.d/conda.sh
conda activate apoorvaenv
python run.py 

