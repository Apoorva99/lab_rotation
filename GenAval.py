#Generating Avalanches

import numpy as np
from latticeActivity import *
from GenGraph import *
import random

def av_gen(m,n,sigma,pext,p0,num_neigh,num_av,ptype,self_exciteP,self_excite_neigh,counter, prw):

    # m*n is the lattice size
    # ref is refractory period
    # sigma is branching parameter
    # pext is the probability for spontaneous firing
    # num_neigh is the number of nearest neighbors which should be considered in model
    # num_av is the number of avalanches which want to generate
    # ptype (is string) shows which type of probability distribution we should consider for neighboring activation
    # self_exciteP if 1 shows we consider self_excitation in probabilities
    # self_excite_neigh if 1 shows we have self-excitation in model (each cell is the first neighbor of itself)
    # direc shows the directory in which we should save the results
    # counter is used for saving avalanche sizes/lifetimes
    # prw probability of rewiring


    if ptype == 'custom_scale_free':
        pneigh = []
        G = get_scale_free(m,n,distance_metric,5,-0.1,1)
        neigh_all = []
        total_deg = 0
        for f in G.nodes():
            total_deg = total_deg + G.degree(f)
        avg_deg = total_deg/(m*n)
        for i in G.nodes():
            neigh_i = [n for n in G.neighbors(i)]
            neigh_all.append([[i],neigh_i])
            pneigh.append((sigma-p0)/(avg_deg+1))
        p = [pext, p0, pneigh]
        # neigh_all = rewire_uniform(n, m, num_neigh, neigh_all, prw)
r

    #center = sorted(G.degree, key=lambda x: x[1], reverse=True)[0][0]
    #center = int(np.ceil(m/2)*n + np.ceil(n/2))
    center = random.randint(0,(m*n)-1)
    avalanche_size = []
    avalanche_lifetime = []
    lattice_activity = []
    m_conv = []
    numActive_all = []
    m_real = []
    save_counter = 0

    for i in range(num_av):
        save_counter = save_counter+1
        s = np.zeros(n*m)
        s[center] = 1
        total_active = 1
        t = 0
        while(np.sum(s>0) > 0):
            lattice_activity.append(s)
            s_old = s
            s = np.zeros(n*m)
            pot_s = np.zeros(n*m) # potential number of active units without coalesence
            for k in range(0,len(s_old)):
                s[k],pot_s[k] = update_network_states_ref0_conv(k,m,n,s_old,p,neigh_all)

            t = t + 1
            if np.sum(s) > 0:
                m_conv.append((np.sum(pot_s) - np.sum(s))/np.sum(s_old))
                numActive_all.append(np.sum(s_old))
                total_active = total_active + np.sum(s>0)

            if total_active > 10**10: # for killing large avalanches in case of supercriticality
                s = np.zeros(n*m)

                m_real.append(np.sum(s)/np.sum(s_old))
               #print(np.sum(s))
               #print(np.sum(s_old))
               #print(np.sum(pot_s))
               #print('--------------------------')

        avalanche_size.append(total_active)
        avalanche_lifetime.append(t)

        if save_counter == counter:
            #print('avalanche ',i+1)
            save_counter = 0

    return numActive_all, m_conv, avalanche_size, avalanche_lifetime, m_real, lattice_activity
