from GenAval import *
import numpy as np
import os

#Parameters
m = 32
n = 32
sigma = 0.851
num_neigh = 1
pext = 0
p0 = 0.5
num_av = 10000
ptype = 'custom_scale_free'
self_exciteP = 1
self_excite_neigh = 1
counter = num_av
prw = 0.3
total_runs = 500
exp_name = 'spatial'

direc_size = 'results/avalanches/size/'
direc_time = 'results/avalanches/time/'
direc = 'results/spikes/'
    

if not os.path.exists(direc_size):
   os.makedirs(direc_size)
   os.makedirs(direc_time)
   os.makedirs(direc+'act/')
   os.makedirs(direc+'mconv/')
   os.makedirs(direc+'mreal/')
   os.makedirs(direc+'LatticeAct/')


for i in range(1,total_runs+1):
    run = i
    print(run)



    act,mconv, avsize, avtime, mreal, latticeAct  = av_gen(m,n,sigma,pext,p0,num_neigh,num_av,ptype,self_exciteP,self_excite_neigh,counter, prw)
    

    # np.save(direc+'org/'+str(run)+'_numAct_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),act)
    # np.save(direc+'sh/'+str(run)+'_mconv_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),mconv)
    # np.save(direc+'sh/'+str(run)+'_mconv_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),mreal)
    # np.save(direc+'sh/'+str(run)+'_mconv_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),latticeAct)
    # np.save(direc_size+str(run)+'_avsize_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),avsize)
    # np.save(direc_time+str(run)+'_avsize_'+ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),avtime)
    
    np.save(direc+'act/'+str(run)+'_numAct_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),act)
    np.save(direc+'mconv/'+str(run)+'_mconv_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),mconv)
    np.save(direc+'mreal/'+str(run)+'_mreal_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),mreal)
    np.save(direc+'LatticeAct/'+str(run)+'LatticeAct'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),latticeAct)
    np.save(direc_size+str(run)+'_avsize_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),avsize)
    np.save(direc_time+str(run)+'_avtime_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name),avtime)
    

path = 'results/spikes1/'
path_av_size = 'results/avalanches/size/'
path_av_time = 'results/avalanches/time/'

act = np.asarray([]) # total number of active units
mconv = np.asarray([]) # coalesence (loss of BR)
mreal = np.asarray([])
avsize = np.asarray([]) # avalanche sizes
avtime = np.asarray([])
lattactivity = np.asarray([])

num_files = total_runs+1
for i in range(1,num_files,1):
    filename = 'act/'+str(i)+'_numAct_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name)+'.npy'
    act = np.concatenate((act,np.load(path+filename)))
    
for i in range(1,num_files,1):
    filename = 'mconv/'+str(i)+'_mconv_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name)+'.npy'
    mconv = np.concatenate((mconv,np.load(path+filename)))

# for i in range(1,num_files,1):
#     filename = 'mreal/'+str(i)+'_mreal_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name)+'.npy'
#     mconv = np.concatenate((mconv,np.load(path+filename)))
    
for i in range(1,num_files,1):
    print(i)
    filename = str(i)+'_avsize_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name)+'.npy'
    avsize = np.concatenate((avsize,np.load(path_av_size+filename)))

for i in range(1,num_files,1):
    print(i)
    filename = str(i)+'_avtime_'+'size' +str(m)+'_sig'+str(sigma)+'_exp'+str(exp_name)+'.npy'
    avtime = np.concatenate((avtime,np.load(path_av_time+filename)))


# compute average and std coalesence conditioned on number of active units    
act = np.asarray(act)
mconv = np.asarray(mconv)
act_uniq = np.unique(act)

avg_mconv_vsAct = []
std_mconv_vsAct = []
num_mconv_vsAct = []
for i in range(len(act_uniq)):
    avg_mconv_vsAct.append(np.mean(mconv[act==act_uniq[i]]))
    std_mconv_vsAct.append(np.std(mconv[act==act_uniq[i]]))
    num_mconv_vsAct.append(len(mconv[act==act_uniq[i]]))
    
avg_mconv_vsAct = np.asarray(avg_mconv_vsAct)
std_mconv_vsAct = np.asarray(std_mconv_vsAct)


path_save = 'results/coalescence/'

if not os.path.exists(path_save):
   os.makedirs(path)


np.save(path_save + 'mconv_' + ptype +str(prw)+'_numNei'+str(num_neigh)+'_size' +str(m)+'_sig'+str(sigma)+ '_ps' + str(p0)+'_exp'+str(exp_name),\
     [avg_mconv_vsAct, std_mconv_vsAct, act_uniq, num_mconv_vsAct, np.mean(mconv), np.std(mconv), avsize, avtime])
