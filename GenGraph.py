#Generates Network

import numpy as np
import networkx as nx

#Calculate euclidean distance with periodic boundary counditions
def distance_metric(x0, x1, dimensions=2):

    '''
    inputs-
    x0: coordinates of first node
    x1: coordinates of second node
    dimensions: dimension of the space 

    returns-
    distance between point x0 and x1
    '''

    delta = np.abs(np.array(x0) - np.array(x1))
    delta = np.where(delta > 0.5 * dimensions, delta - dimensions, delta)
    return ((delta ** 2).sum(axis=-1))



#Generates connections to be made based on degree and node distribution
def preferential_attachment(current_node, potential_connections, G, distance_weights, degree_weights,distance_metric,m):

    '''
    inputs-
    current node: node being currently added
    potential connections: all the preceeding nodes
    G: Graph in networkx
    distance_weights: how much importance to give to distance between nodes for possibility of connection
    degree_weights: how much importance to give to degree of nodes for possibility of connection
    distance_metric: function to calculate distance between nodes
    m: number of nodes to add on one step

    returns-
    connections: new connections to be made

    '''

    # Calculate distances between current node and potential connections
    distances = [distance_metric(G.nodes[current_node]["pos"], G.nodes[node]["pos"]) for node in potential_connections]
    # Calculate degrees of potential connections
    degrees = [G.degree(node) for node in potential_connections]
    # Combine distance and degree information into a single score
    scores = [(d**(distance_weights/2)) * (k**degree_weights) for (d,k) in zip(distances, degrees)]
    # Normalize scores
    scores_sum = sum(scores)
    if scores_sum > 0:
        p = [score / scores_sum for score in scores]
    else:
        distances = [distance_metric(G.nodes[0]["pos"], G.nodes[node]["pos"]) for node in list(G.nodes())[1:]]
        dis = [(d**distance_weights) for d in distances]
        dis_sum = sum(dis)
        p = [dist/dis_sum for dist in dis]
        potential_connections = list(G.nodes())[1:]
    
    # Select m connections using the preferential attachment process
    connections = np.random.choice(potential_connections, size=m, p=p)

    return connections

def get_scale_free(num_rows,num_cols,distance_metric,m,distance_weights,degree_weights):

    '''
    inputs-
    num_rows*num_cols: size of network
    distance_metric: function to calculate dista
    distance_weights: how much importance to give to distance between nodes for possibility of connection
    degree_weights: how much importance to give to degree of nodes for possibility of connection

    returns-
    G: Final Graph

    '''
    # Create an empty graph object
    G = nx.Graph()

    # Set the dimensions of the lattice
    num_rows = num_rows
    num_cols = num_cols
    m = m


    # Add nodes to the graph
    G.add_nodes_from(range(num_rows * num_cols))

    #Give each node a random position
    pos = nx.random_layout(G)

    # Set the 'pos' attribute for each node according to its position
    for i in range(num_rows * num_cols):
        G.nodes[i]['pos'] = pos[i]

    # # Iterate over the nodes, adding new connections at each time step
    for i in range(num_rows*num_cols):
        # print(i)
        # Generate a list of potential connections
        potential_connections = [j for j in range(i)]
        # Select connections using the preferential attachment function
        connections = preferential_attachment(i, potential_connections, G, distance_weights, degree_weights,distance_metric,m)
        # Add the connections to the graph
        G.add_edges_from((i, j) for j in connections)

    return G
